import sqlite3

con = sqlite3.connect('phonebook.sqlite3')
cur = con.cursor()

while(True):
    select = input("Enter 1 To Insert New Contact\nEnter 2 To View Contacts\nEnter 0 To Exit: ")
    if(select == '0'):
        break
    if(select == '1'):
        while(True):
            innerSelect = input("Enter 9 To Go Back: ")
            if(innerSelect == '9'):
                break
            name = input("Enter Name: ")
            phoneNumber = input("Enter Phone Number: ")
            cur.execute('INSERT INTO contacts (name, phone) VALUES (?,?)',(name, phoneNumber))  
            con.commit()
    if(select == '2' ):
        cur.execute('SELECT name, phone FROM contacts')
        for row in cur:
            print("Name: ", row[0], "\nPhone Number: ", row[1])
            print("------------------------------------------------")

con.close()
